## Purpose
The purpose of the program is to find words in a crossword puzzle. Input formating restrictions are detailed in the "Input Format" section below.

## Instructions
First, download "JimmyLoweEnlightenChallenge.py" to your computer.  Because this solution was developed on Python 3.9.4, the next step is to downloaded the python package here: https://www.python.org/downloads/.  Make sure to download it to the same folder that contains JimmyLoweEnlightenChallenge.py!  If you would like, you can download test.txt which contains a 10x11 puzzle. Finally, run the program from command prompt, and put in the name of your test file when it asks you.  The program should return the solution in the format outlined in the "Output Format" section, along with the run time.  If anything doesn't work the way it should, feel free to send me an email at jblowe@vt.edu!

## Methodology
The solution was simple enough to encapsulate most of the work into just one function. Because of this, I decided not to create a class, as creating a class to store the function was unneccessary and would mostly cause confusion. To solve the crossword, I decided to take a brute force approach in order to sacrifice runtime to keep the code simple and readable. One area to optimize the solution for larger puzzles would be to create a lookup table that stores the possible directions from each position under the "universal lookup tables" section.  In 10x11 or smaller puzzles, my testing showed that a brute force approach performed better than an approach with a lookup-table, which is why I did not include it in the uploaded solution.  Also I couldn't tell if I was supposed to include a run time, so I included it anyways because it doesn't affect performance or readability. In order to maximize maintainability and code mobility, the function is entirely self-dependent so it could be copied and pasted elsewhere without causing issues. 

## Performance
I tested the code on a few crossword puzzles ranging in size between 3x3 and 10x11, and it consistently performed in under 0.007 seconds for each size. Potential areas to imrpove performance on larger tests would be to generate a lookup table or to cache the values, or to look for all of the words in one pass.

## Input Format
The program accepts the name of a file as input. The file should be an ASCII text file containing the word search board along with the words that need to be found. 

The file must contain three parts. The first part is the first line, and specifies the number of rows and columns in the grid of characters, separated by an 'x'. The second part provides the grid of characters in the word search. The third part in the file specifies the words to be found.

The first line indicates how many following lines in the file contain the rows of characters that make up the word search grid. Each row in the word search grid must have the specified number of columns of characters, each separated with a space. The remaining lines in the file specify the words to be found.

An example file is as follows:

```
3x3
A B C
D E F
G H I
ABC
AEI
```

The program assumes that the input files are correctly formatted, and error handling for invalid input files (aside from being unable to find the file) is ommitted.

### Output Format
The output specifies the word found, along with the indices where the beginning and ending characters of the word are located in the grid. A single space character will separate the word from the beginning and ending indices. The order of the words in the output remains the same as the order of the words specified in the input file. The program outputs to the console.

The word search grid row and column numbers are used to identify the location of individual characters in the board. For example, row 0 column 0 (represented as `0:0`) is the top-left character of a 3x3 board.  The bottom-right corner of a 3x3 board would be represented as `2:2`.

```
ABC 0:0 0:2
AEI 0:0 2:2
```
