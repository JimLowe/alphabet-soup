# Jimmy Lowe's Enlighten Alphabet Soup Coding Challenge, 4/7/2021

import time

# Get the name of the file
print("Please input the name of the file here: ")
filename = input()

#  Try to open the file, have a graceful error message if it's not found
try:
    file = open(filename)
except Exception as E:
    print("Error: Please make sure the crossword is in the same folder as this program")
    exit()

# Keep track of runtime
startTime = time.time()

# ------------------------- Pull the information out of the file -------------------------

# Get the number of rows and columns
rowAndHeight = file.readline()
rowNum = int(rowAndHeight[: rowAndHeight.find("x")])
height = int(rowAndHeight[rowAndHeight.find("x")+1:])

# Put the puzzle into an array
puzzleArr = [[0 for y in range(height)] for x in range(rowNum)]
for currentRow in range(rowNum):
    currentLine = file.readline()
    currentX = 0
    for character in currentLine:
        if character != " " and character != "\n":
            puzzleArr[currentRow][currentX] = character
            currentX += 1

# Get the words I'm searching for
answerWords = []
for line in file:
    line = "".join(line.split())
    answerWords.append(line)

# Close the file
file.close()

# ------------------------- Set universal lookup tables --------------------------
''' 
    0 - up
    1 - right
    2 - down
    3 - left
    4 - diagonal up left
    5 - diagonal up right
    6 - diagonal down left
    7 - diagonal down right
'''

# Tuples for each given direction (index corresponds to direction above)


# ------------------------- Create and Run My Function -------------------------


# Function to find a given word in the crossword
def findWord(arr, word):
    firstLetter = word[0]
    numOfCols = len(arr)
    numOfRows = len(arr[0])
    directions = [(0, -1), (1, 0), (0, 1), (-1, 0), (-1, -1), (-1, 1), (1, -1), (1, 1)]
    for y, line in enumerate(arr):
        for x, letter in enumerate(line):
            # Only check if it matches the first letter, to improve runtime
            if letter == firstLetter:
                length = len(word)
                # Get a word in each direction of the same length, and see if it's the answer
                for direction in directions:
                    n = 1
                    testWord = letter
                    newX = x + direction[0]
                    newY = y + direction[1]
                    while 0 <= newX < numOfRows and 0 <= newY < numOfCols and n < length:
                        testWord += arr[newY][newX]
                        newX += direction[0]
                        newY += direction[1]
                        n += 1
                    # Return in desired format
                    if testWord == word:
                        return word + " " + str(y) + ":" + str(x) + " " + str(newY-direction[1]) + ":" + \
                            str(newX-direction[0])
    # Make it clear if a word does not exist
    return "Could not find " + word


# Find each word and print results
for word in answerWords:
    print(findWord(puzzleArr, word))
print("Total Runtime: " + str(round(time.time()-startTime, 5)) + " Seconds")
